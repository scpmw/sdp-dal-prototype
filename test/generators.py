import random
import numpy as np
import rascil.processing_components.simulation as sim
import rascil.processing_components.util as util


class Generator(object):

    def __init__(self):
        pass

    def generate(self, arguments: dict) -> object:
        pass


class UVWGenerator(Generator):

    def __init__(self):
        super().__init__()
        random.seed()

    def generate(self, arguments: dict) -> object:
        xyz = np.array([row[1] for row in sim.create_named_configuration(name='LOW').data])       # xyz = np.random.random_sample((3,))
        rand_ha = 2 * np.pi * random.random()  # HA between 0 and 2 pi radians
        rand_dec = (2 * random.random() - 1) * np.pi / 2  # DEC between -pi/2 and pi/2
        uvw = util.xyz_to_uvw(xyz, ha=rand_ha, dec=rand_dec)
        return uvw
