# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(

    # Project name
    name="sdp-dal-prototype",

    # Code version
    version="0.1",

    # Authors
    author="Matthieu Marseille",
    author_email="matthieu.marseille@thales-services.fr",

    # Description
    description="SKA SDP DAL prototype",

    # URL
    # TODO: put URL

    # Python version
    python_requires='>=3.6',

    # Licence
    license='Apache License Version 2.0',

    # Requirements
    install_requires=['numpy', 'pyarrow', 'rascil']

)
