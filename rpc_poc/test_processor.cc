
#include "processor.h"

class TestProcessor : public Processor {
 protected:
    virtual arrow::Status ProcessCall(
        const std::string &proc_func,
        std::shared_ptr<arrow::RecordBatch> batch) override;

};

arrow::Status TestProcessor::ProcessCall(const std::string &proc_func,
                                         std::shared_ptr<arrow::RecordBatch> batch)
{

    // Read parameters
    int32_t val;
    ARROW_RETURN_NOT_OK(
        ReadValue<arrow::Int32Type>(batch->GetColumnByName("val"), &val));
    std::vector<plasma::ObjectID> obj_ids;
    std::shared_ptr<arrow::NumericTensor<arrow::Int32Type> > in_tensor;
    ARROW_RETURN_NOT_OK(GetParameterTensor(batch, "in", &in_tensor));
    const int32_t size = in_tensor->size();
    plasma::ObjectID oid_out;
    ARROW_RETURN_NOT_OK(ReadObjectID(batch->GetColumnByName("out"), &oid_out));

    // "Compute" result tensor
    std::vector<int32_t> out(size);
    //std::cout << "Writing to " << oid_out.hex() << ":";
    const int32_t *in_data = reinterpret_cast<const int32_t *>(in_tensor->raw_data());
    for (int i = 0; i < size; i++) {
        out[i] = in_data[i] + val;
        //std::cout << " " << out[i];
    }
    //std::cout << std::endl;

    // Wrap as tensor
    auto out_data = arrow::Buffer::Wrap(out);
    arrow::Tensor out_tensor(arrow::int32(), out_data, {size});

    // Write output object
    int64_t tensor_size;
    ARROW_RETURN_NOT_OK(arrow::ipc::GetTensorSize(out_tensor, &tensor_size));
    std::shared_ptr<arrow::Buffer> out_buf;
    ARROW_RETURN_NOT_OK(client_->Create(oid_out, tensor_size, NULL, 0,
                                        &out_buf));
    arrow::io::FixedSizeBufferWriter writer(out_buf);
    int32_t metadata_length; int64_t tensor_size_written;
    ARROW_RETURN_NOT_OK(arrow::ipc::WriteTensor(out_tensor, &writer,
                                                &metadata_length,
                                                &tensor_size_written));
    ARROW_RETURN_NOT_OK(client_->Seal(oid_out));

    // Release the output object manually. Not sure why this is required...
    ARROW_RETURN_NOT_OK(client_->Release(oid_out));

    return arrow::Status::OK();
}

int main(int argc, char *argv[])
{

    // Make sure we get Plasma path + Object ID prefix
    bool valid = true;
    plasma::ObjectID prefix; int prefix_len = 0;

    // Parse (hexadecimal) object ID prefix
    if (argc >= 3) {
        prefix_len = ParseHexObjectID(argv[2], &prefix);
        valid = (prefix_len > 0);
    } else {
        valid = false;
    }

    if (!valid) {
        std::cerr
            << "Please provide plasma connector and ID prefix!" << std::endl
            << "  Example: ./processor /tmp/plasma 00000000" << std::endl;
        return 1;
    }

    TestProcessor proc;
    ARROW_CHECK_OK(proc.Connect(argv[1], prefix, prefix_len));

    while(true) {
        ARROW_CHECK_OK(proc.Process());
    }
    // Disconnect the client.
    ARROW_CHECK_OK(proc.Disconnect());

    return 0;
}
