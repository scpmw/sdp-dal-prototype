
#include "processor.h"

#include <iostream>

Processor::~Processor()
{
}

arrow::Status Processor::Connect(std::string plasma_path,
                                 plasma::ObjectID prefix,
                                 int prefix_length)
{

    // Create connection
    auto client = std::make_shared<plasma::PlasmaClient>();
    ARROW_RETURN_NOT_OK(client->Connect(plasma_path));

    // Subscribe to object updates
    int sub_fd;
    ARROW_RETURN_NOT_OK(client->Subscribe(&sub_fd));

    // Get notifications.
    ARROW_RETURN_NOT_OK(client->List(&obj_table_));

    // Set
    client_ = client;
    sub_fd_ = sub_fd;
    prefix_ = prefix;
    prefix_length_ = prefix_length;
    return arrow::Status::OK();
}

arrow::Status Processor::Process()
{

    // Get next notification
    plasma::ObjectID oid;
    auto entry = std::unique_ptr<plasma::ObjectTableEntry>(
        new plasma::ObjectTableEntry);
    ARROW_RETURN_NOT_OK(client_->GetNotification(
        sub_fd_, &oid, &entry->data_size, &entry->metadata_size));

    // Deletion?
    if (entry->data_size < 0) {
        obj_table_.erase(oid);
        return arrow::Status::OK();
    }
    assert(obj_table_.count(oid) == 0);

    // Set in table
    //std::cout << oid.hex() << " created (" << entry->data_size << " + "
    //          << entry->metadata_size << " bytes)" << std::endl;
    obj_table_[oid] = std::move(entry);

    // Has prefix? Attempt to read as calls
    if (memcmp(oid.data(), prefix_.data(), prefix_length_) == 0) {
        ARROW_RETURN_NOT_OK(ReadCalls(oid));
    }

    // Calls waiting for this input object?
    auto range = delayed_calls_.equal_range(oid);
    for (auto it = range.first; it != range.second; ++it) {
        std::vector<int> in_obj_cols, out_obj_cols;
        auto call_oid = it->second.first; auto batch = it->second.second;
        FindInOutPars(batch->schema(), &in_obj_cols, &out_obj_cols);
        ARROW_RETURN_NOT_OK(ReadCall(call_oid, batch, in_obj_cols, out_obj_cols));
    }
    delayed_calls_.erase(oid);

    return arrow::Status::OK();
}

int ParseHexObjectID(const std::string &str, plasma::ObjectID *prefix)
{

    // Validate
    if (str.length() % 2 != 0)
        return 0;
    if (str.length() / 2 > plasma::kUniqueIDSize)
        return 0;

    // Read bytes
    for (size_t i = 0; i < str.length() / 2; i++) {
        std::stringstream strm(str.substr(i*2, 2));
        int num;
        strm >> std::hex >> num;
        prefix->mutable_data()[i] = num;
    }

    return str.length() / 2;
}

void Processor::FindInOutPars(std::shared_ptr<arrow::Schema> schema,
                              std::vector<int> *in_obj_cols,
                              std::vector<int> *out_obj_cols)
{
    // Find input/output parameters
    for (int col = 0; col < schema->num_fields(); col++) {
        auto field = schema->field(col);

        // Check that field has Object ID type and has parameter kind
        // metadata associated with it
        int meta;
        if (!field->HasMetadata() ||
            !field->type()->Equals(object_id_type) ||
            (meta = field->metadata()->FindKey(proc_par_meta)) < 0) continue;

        // Check parameter kind, add to appropriate list
        auto parkind = field->metadata()->value(meta);
        if (parkind == proc_par_meta_in) {
            in_obj_cols->push_back(col);
        }
        if (parkind == proc_par_meta_out) {
            out_obj_cols->push_back(col);
        }
    }
}

arrow::Status Processor::ReadCalls(const plasma::ObjectID &object_id)
{

    std::cout << "Processing " << object_id.hex() << std::endl;

    std::vector<plasma::ObjectBuffer> object_buffers;
    ARROW_RETURN_NOT_OK(client_->Get({object_id}, -1, &object_buffers));

    // Try to open stream for reading record batches. This will read
    // the schema from the prefix, which tells us the function to call
    // as well as parameter schemas.
    arrow::io::BufferReader reader(object_buffers[0].data);
    std::shared_ptr<arrow::ipc::RecordBatchReader> batch_reader;
    ARROW_RETURN_NOT_OK(arrow::ipc::RecordBatchStreamReader::Open(
        &reader, &batch_reader));

    // Identify in/out object parameters
    std::vector<int> in_obj_cols, out_obj_cols;
    FindInOutPars(batch_reader->schema(), &in_obj_cols, &out_obj_cols);

    // Read function call record batch(es)
    while(true) {

        // At end of stream? Need to check this manually because
        // apparently ReadNext just segfaults otherwise... There must
        // be a more elegant way to do this?
        const auto peek = reader.Peek(8);
        const auto end_marker = std::string("\377\377\377\377\0\0\0\0", 8);
        if (!peek.ok() || *peek == end_marker) break;

        // Read next record batch
        std::shared_ptr<arrow::RecordBatch> batch;
        ARROW_RETURN_NOT_OK(batch_reader->ReadNext(&batch));

        // Go through rows (i.e. individual calls)
        for (int row = 0; row < batch->num_rows(); row++) {
            std::shared_ptr<arrow::RecordBatch> batch_row = batch->Slice(row);
            ARROW_RETURN_NOT_OK(ReadCall(object_id, batch_row,
                                         in_obj_cols, out_obj_cols));
        }
    }
    return arrow::Status::OK();
}

arrow::Status Processor::ReadObjectID(std::shared_ptr<arrow::Array> arr,
                                      plasma::ObjectID *obj_id)
{
    const uint8_t *oid_str;
    ARROW_RETURN_NOT_OK((ReadValue<arrow::FixedSizeBinaryType>
                         (arr, &oid_str)));
    memcpy(obj_id->mutable_data(), oid_str, plasma::kUniqueIDSize);
    return arrow::Status::OK();
}

template <class T>
arrow::Result<std::string> ReadMetadata(std::shared_ptr<T> schema,
                                        std::string key)
{
    // Check key exists in metadata
    int i;
    if (!schema->HasMetadata() || (i = schema->metadata()->FindKey(key)) < 0) {
        arrow::Status status(arrow::StatusCode::TypeError,
                             "Expected meta data " + key + " not found!");
        return arrow::Result<std::string>(status);
    }
    // Return
    return arrow::Result<std::string>(schema->metadata()->value(i));
}

template <class T>
std::string ReadMetadata(std::shared_ptr<T> schema,
                         std::string key, std::string default_)
{
    // Check key exists in metadata
    int i;
    if (!schema->HasMetadata() || (i = schema->metadata()->FindKey(key)) < 0) {
        return default_;
    }
    // Return
    return schema->metadata()->value(i);
}

arrow::Status Processor::ReadCall(const plasma::ObjectID &call_oid,
                                  std::shared_ptr<arrow::RecordBatch> batch,
                                  const std::vector<int> &in_obj_cols,
                                  const std::vector<int> &out_obj_cols)
{

    // Get function name
    ARROW_ASSIGN_OR_RAISE(std::string proc_func,
                          ReadMetadata(batch->schema(), proc_func_meta))

    // Check that outputs are not satisfied (i.e. that this
    // request has not been processed already)
    bool already_processed = true;
    for (int col : out_obj_cols) {
        plasma::ObjectID oid;
        ARROW_RETURN_NOT_OK(ReadObjectID(batch->column(col), &oid));
        if (obj_table_.find(oid) == obj_table_.end()) {
            already_processed = false;
            break;
        }
    }
    if (already_processed) {
        std::cout << "Outputs for " << proc_func << " already sealed, ignoring."
                  << std::endl;
        return arrow::Status::OK();
    }

    // Check that inputs are present
    for (int col : in_obj_cols) {
        plasma::ObjectID oid;
        ARROW_RETURN_NOT_OK(ReadObjectID(batch->column(col), &oid));
        // Not present? Delay
        if (obj_table_.find(oid) == obj_table_.end()) {
            //std::cout << "Inputs for " << proc_func << " missing, delaying."
            //          << std::endl;
            // Add to delayed calls, increase ref count on call plasma object
            delayed_calls_.insert(std::make_pair(oid,std::make_pair(call_oid,
                                                                    batch)));
            return arrow::Status::OK();
        }
    }

    // Process the call
    //std::cout << "Processing call to " << proc_func << "..." << std::endl;
    ARROW_RETURN_NOT_OK(ProcessCall(proc_func, batch));

    return arrow::Status::OK();
}

arrow::Status
Processor::GetParameterBuf(std::shared_ptr<arrow::RecordBatch> batch,
                           const std::string &par_name,
                           std::shared_ptr<arrow::Buffer> *buf)
{

    // Make sure column exists and has right kind
    auto field = batch->schema()->GetFieldByName(par_name);
    ARROW_RETURN_IF(!field, arrow::Status(arrow::StatusCode::IndexError,
                                          "Field " + par_name + " not found!"));
    ARROW_ASSIGN_OR_RAISE(auto par, ReadMetadata(field, proc_par_meta))
    ARROW_RETURN_IF(par != proc_par_meta_in,
                    arrow::Status(arrow::StatusCode::Invalid,
                                  "Field " + par_name + " not declared as input!"));

    // Read
    plasma::ObjectID oid;
    ARROW_RETURN_NOT_OK(ReadObjectID(batch->GetColumnByName(par_name), &oid));
    std::vector<plasma::ObjectBuffer> object_buffers;
    ARROW_RETURN_NOT_OK(client_->Get({oid}, 0, &object_buffers));

    // Return
    *buf = std::move(object_buffers[0].data);
    return arrow::Status::OK();
}

