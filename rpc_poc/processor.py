
import abc

import pyarrow.plasma as plasma
import pyarrow

OBJECT_ID_SIZE = 20
OBJECT_ID_TYPE = pyarrow.binary(OBJECT_ID_SIZE)

def parse_hex_objectid(oid_str):

    # Validate
    if len(oid_str) % 2 != 0:
        raise ValueError("Object ID string must have even length!")
    if len(oid_str) // 2 > OBJECT_ID_SIZE:
        raise ValueError("Object ID can have {} bytes at maximum!".format(
            OBJECT_ID_SIZE))

    # Parse prefixes
    oid = bytearray()
    for i in range(len(oid_str)//2):
        oid.append(int(oid_str[2*i:2*i+2], 16))
    return oid

def object_id_hex(oid):
    if isinstance(oid, plasma.ObjectID):
        oid = oid.binary()
    return "".join(["{:02x}".format(c) for c in oid])

def objectid_generator(prefix):
    # Pad prefix to length
    oid = bytearray(prefix)
    while len(oid) < kUniqueIDSize:
        oid.append(0)
    # Generate IDs
    byte_count = kUniqueIDSize - len(prefix)
    for i in range(2**(8*byte_count)):
        for k in range(byte_count):
            oid[-k-1] = (i >> 8*k) % 256
        yield bytes(oid)

PROC_FUNC_META = b"proc:func";
PROC_PAR_META = b"proc:par";
PROC_PAR_META_IN = b"in";
PROC_PAR_META_OUT = b"out";

class Processor(metaclass = abc.ABCMeta):

    def __init__(self, plasma_path, prefix):

        # Create connection
        self._client = plasma.connect(plasma_path)

        # Subscribe to event updates
        self._client.subscribe()

        # Get list of objects
        self._obj_table = self._client.list()

        # Initialise
        self._prefix = prefix
        self._delayed = {}

    def process(self):

        # Get next notification
        oid, data_size, metadata_size = self._client.get_next_notification()

        # Deletion?
        if data_size < 0:
            del self._obj_table[oid]
            return

        # Set in object table
        self._obj_table[oid] = {
            'data_size': data_size,
            'metadata_size': metadata_size
        }

        # Has call prefix? Request and attempt to process
        if oid.binary().startswith(self._prefix):
            self._read_calls(oid)

        # Delayed call waiting for this input?
        if oid in self._delayed:
            for call_oid, batch in self._delayed[oid]:
                in_obj_cols, out_obj_cols = self._find_in_out_pars(batch.schema)
                self._read_call(call_oid, batch, in_obj_cols, out_obj_cols)
            del self._delayed[oid]

    def _read_calls(self, oid):

        # Receive objects
        print("Processing {}".format(object_id_hex(oid)))
        object_buffers = self._client.get_buffers([oid])

        # Read as a record batch
        batch_reader = pyarrow.RecordBatchStreamReader(
            pyarrow.BufferReader(object_buffers[0]))

        # Identify in+out parameters
        in_obj_cols, out_obj_cols = self._find_in_out_pars(batch_reader.schema)

        # Get batches
        for batch in batch_reader:

            # Go through rows
            for row in range(batch.num_rows):
                batch_row = batch.slice(row, 1)
                self._read_call(oid, batch_row, in_obj_cols, out_obj_cols)

    def _find_in_out_pars(self, schema):
        """ Identify input and output parameters in a schema.
        """

        in_obj_cols = []; out_obj_cols = []
        for col in range(len(schema.names)):
            field = schema.field(col)

            # Check that field has ObjectID type and has parametr kind
            # metadata associated with it
            if field.metadata is None or \
               PROC_PAR_META not in field.metadata or \
               not field.type.equals(OBJECT_ID_TYPE):
                continue

            # Check the concrete parameter kind
            parkind = field.metadata[PROC_PAR_META]
            if parkind == PROC_PAR_META_IN:
                in_obj_cols.append(col)
            elif parkind == PROC_PAR_META_OUT:
                out_obj_cols.append(col)
        return in_obj_cols, out_obj_cols

    def _read_call(self, call_oid, batch, in_obj_cols, out_obj_cols):

        # Get function name
        proc_func = batch.schema.metadata[PROC_FUNC_META].decode('utf8')

        # Check whether some outputs are not satisfied yet (i.e. the
        # request has not been processed already)
        already_processed = True
        for col in out_obj_cols:
            oid = plasma.ObjectID(batch.column(col)[0].as_py())
            if oid not in self._obj_table:
                already_processed = False
                break
        if already_processed:
            print("Outputs for {} already sealed, ignoring".format(proc_func))
            return

        # Check that inputs are present
        for col in in_obj_cols:
            oid = plasma.ObjectID(batch.column(col)[0].as_py())
            if oid not in self._obj_table:
                #print("Input {} for {} missing delaying".format(
                #    object_id_hex(oid), proc_func))
                if oid in self._delayed:
                    self._delayed[oid].append((call_oid, batch))
                else:
                    self._delayed[oid] = [(call_oid, batch)]
                return

        # Process
        self._process_call(proc_func, batch)

    def parameter(self, batch, name, typ=None):
        """Extract parameter from first row of record batch
        :param batch: Record batch containing parameter
        :param name: Name of parameter to extract
        :param typ: Type to check (optional)
        """
        # Look up column name
        col = batch.schema.get_field_index(name)
        # To type check
        if typ is not None and not batch.schema.field(col).type.equals(typ):
            raise TypeError("Column {} has type {}, expected {}!".format(
                name, batch.schema.field(col).type, typ))
        # Get value, convert to Python interpretation
        return batch[col][0].as_py()

    def oid_parameter(self, batch, name):
        """Extract Object ID parameter from first row of record batch.
        :param batch: Record batch containing parameter
        :param name: Name of parameter to extract
        """
        return plasma.ObjectID(self.parameter(batch, name, OBJECT_ID_TYPE))

    def tensor_parameters(self, batch, name_typ_dims):
        """Read tensors referred to via object ID parameters.
        :param batch: Record batch containing parameters
        :param name_typ_dims: Either list of strings or list of tuples
          of form (name, type, dimensionality). If given, type and
          dimensionality will be checked.
        """

        # Collect object IDs
        oids = []
        for name_typ_dim in name_typ_dims:
            if isinstance(name_typ_dim, tuple):
                name = name_typ_dim[0]
            else:
                name = name_typ_dim
            oids.append(self.oid_parameter(batch, name))
            # Make sure it was marked as an input parameter (because
            # of the previous check this virtually guarantees that the
            # call below will not block)
            meta = batch.schema.field(name).metadata
            if meta is None or meta.get(PROC_PAR_META) != PROC_PAR_META_IN:
                raise ValueError("Tensor call parameter {} not marked "+
                                 "as input!".format(name))

        # Batch-request buffers
        bufs = self._client.get_buffers(oids, 100)

        # Read them all
        arrays = []
        for buf, name_typ_dim in zip(bufs, name_typ_dims):
            tensor = pyarrow.read_tensor(pyarrow.BufferReader(buf))
            # Make sure type and dimensionality matches, if given
            if isinstance(name_typ_dim, tuple):
                try:
                    it = iter(name_typ_dim)
                    name = next(it)

                    # Check type
                    typ = next(it)
                    if typ is not None and not tensor.type.equals(typ):
                        raise TypeError("Column {} refers to tensor "+\
                                        "of type {}, expected {}!".format(
                                            name, tensor.type, typ))
                    # Check dimensionality
                    dim = next(it)
                    if dim is not None and tensor.ndim != dim:
                        raise TypeError("Column {} refers to tensor "+\
                                        "of dimensionality {}, expected {}!".format(
                                            name, tensor.ndim, dim))

                except StopIteration:
                    pass

            # Convert to numpy
            arrays.append(tensor.to_numpy())
        return arrays

    def tensor_parameter(self, batch, name, typ=None, dims=None):
        """Read tensor referred to via object ID parameter.
        :param batch: Record batch containing parameters
        :param name: Name of parameter to extract
        :param typ: Tensor value type to check (optional)
        :param dims: Tensor dimensionality to check (optional)
        """
        return self.tensor_parameters(batch, [(name, typ, dims)])[0]

    @abc.abstractmethod
    def _process_call(self, proc_func, batch):
        pass

class LogProcessor(Processor):

    def _process_call(self, proc_func, batch):

        field_strs = []
        for col in range(len(batch.schema.names)):
            field = batch.schema.field(col)
            metadata_str = ''
            if field.metadata is not None:
                metadata_str = dict(field.metadata)
            field_strs.append( "{}={} {}".format(
                field.name, batch.column(col)[0], metadata_str))
        print("{}({})".format(proc_func, ",\n\t".join(field_strs)))
