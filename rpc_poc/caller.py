
import pyarrow.plasma as plasma
import pyarrow

import numpy
import sys
import time

# Make sure we get enough parameters
if len(sys.argv) != 4:
    print("Usage: python caller.py /tmp/plasma 30303030 31303030")
    exit(1)
_, plasma_path, call_prefix, data_prefix = sys.argv

# Parse prefixes
kUniqueIDSize = 20
def parse_hex_objectid(oid_str):
    if len(oid_str) % 2 != 0:
        raise ValueError("Hexadecimal string must be even-sized!")
    if len(oid_str) // 2 > kUniqueIDSize:
        return 0;
    oid = bytearray()
    for i in range(len(oid_str)//2):
        oid.append(int(oid_str[2*i:2*i+2], 16))
    return oid

def object_id_hex(oid):
    if isinstance(oid, plasma.ObjectID):
        oid = oid.binary()
    return "".join(["{:02x}".format(c) for c in oid])

def objectid_generator(prefix):
    # Pad prefix to length
    oid = bytearray(prefix)
    while len(oid) < kUniqueIDSize:
        oid.append(0)
    # Generate IDs
    byte_count = kUniqueIDSize - len(prefix)
    for i in range(2**(8*byte_count)):
        for k in range(byte_count):
            oid[-k-1] = (i >> 8*k) % 256
        yield bytes(oid)
call_oids = objectid_generator(parse_hex_objectid(call_prefix))
data_oids = objectid_generator(parse_hex_objectid(data_prefix))

# Call schema: Two non-null object parameters, in and out
call_schema = pyarrow.schema([
    ('val', pyarrow.int32(), False),
    ('in', pyarrow.binary(20), False, {'proc:par': 'in'}),
    ('out', pyarrow.binary(20), False, {'proc:par': 'out'})
], metadata={'proc:func': 'scalar_add_tensor'})

# Generate call record batches
ncalls = 1000
data = []
vals = []
for i in range(ncalls):
    vals.append(i)
    data.append(next(data_oids))
data.append(next(data_oids))
data_in = data[:-1]
data_out = data[1:]
batch = pyarrow.record_batch([
    pyarrow.array(vals,      pyarrow.uint32()),   # val
    pyarrow.array(data_in,   pyarrow.binary(20)), # in
    pyarrow.array(data_out,  pyarrow.binary(20))  # out
], schema=call_schema)
batches = [batch]

# Write to an IPC stream (will contain schema + 2 record batches)
sink = pyarrow.BufferOutputStream()
writer = pyarrow.RecordBatchStreamWriter(sink, call_schema)
for batch in batches:
    writer.write_batch(batch)
writer.close()

# Write calls to object store
client = plasma.connect(sys.argv[1])
call = next(call_oids)
client.create_and_seal(plasma.ObjectID(call), sink.getvalue().to_pybytes())
print("Wrote {} calls to {}".format(
    list([batch.num_rows for batch in batches]),
    object_id_hex(call)))

# Write numpy array as tensor for initial input
inp = numpy.arange(0, 100000, dtype=numpy.int32)
inp_tensor = pyarrow.Tensor.from_numpy(inp)
tensor_sink = pyarrow.BufferOutputStream()
pyarrow.write_tensor(inp_tensor, tensor_sink)
client.create_and_seal(plasma.ObjectID(data[0]), tensor_sink.getvalue().to_pybytes())
start = time.time()
print("Wrote tensor {} to {}".format(
    inp, object_id_hex(data[0])))

# Wait for output
print("Waiting for result in {}...".format(object_id_hex(data[ncalls])))
out_buf = client.get_buffers([plasma.ObjectID(data[ncalls])]) # This will block
out_tensor = pyarrow.read_tensor(pyarrow.BufferReader(out_buf[0]))
duration = time.time() - start
print("Received:".format(time.time() - start), out_tensor.to_numpy())
print("Took {:.2f} s ({:.2f} ms/call)".format(duration, duration / ncalls * 1000))

# Clean up objects
client.delete([plasma.ObjectID(oid) for oid in data + [call]])
