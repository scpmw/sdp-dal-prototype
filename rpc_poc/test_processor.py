
import sys
import pyarrow
from pyarrow import plasma

import processor

USE_CREATE_AND_SEAL = False

class TestProcessor(processor.Processor):

    def _process_call(self, proc_name, batch):

        # Read parameters
        val = self.parameter(batch, 'val', pyarrow.int32())
        out_obj = self.oid_parameter(batch, 'out')
        in_arr = self.tensor_parameter(batch, 'in', pyarrow.int32(), 1)

        # The actual calculation
        out_arr = in_arr + val

        # Write to output
        if USE_CREATE_AND_SEAL:
            writer = pyarrow.BufferOutputStream()
            pyarrow.write_tensor(pyarrow.Tensor.from_numpy(out_arr), writer)
            self._client.create_and_seal(out_obj, writer.getvalue().to_pybytes())
        else:
            tensor = pyarrow.Tensor.from_numpy(out_arr)
            out_buf = self._client.create(out_obj, pyarrow.get_tensor_size(tensor))
            writer = pyarrow.FixedSizeBufferWriter(out_buf)
            pyarrow.write_tensor(tensor, writer)
            self._client.seal(out_obj)

if len(sys.argv) < 3:
    print("Please provide plasma connector and ID prefix!")
    print("  Example: python processor.py /tmp/plasma 00000000")
    exit(1)

# Create processor, process calls forever
proc = TestProcessor(sys.argv[1], processor.parse_hex_objectid(sys.argv[2]))
while True:
    proc.process()
