

#pragma once

#include <memory>
#include <sstream>
#include <iostream>

#include <arrow/ipc/reader.h>
#include <arrow/status.h>
#include <arrow/array.h>
#include <arrow/io/memory.h>
#include <arrow/util/logging.h>
#include <arrow/util/key_value_metadata.h>
#include <plasma/client.h>

// Data type of an Plasma object ID
const auto object_id_type = arrow::fixed_size_binary(plasma::kUniqueIDSize);

// Meta data names
const std::string proc_func_meta = "proc:func";
const std::string proc_par_meta = "proc:par";
const std::string proc_par_meta_in = "in";
const std::string proc_par_meta_out = "out";

// Meta-data to mark fields as "input" or "output"
const auto in_field = std::shared_ptr<arrow::KeyValueMetadata>(
    new arrow::KeyValueMetadata({proc_par_meta}, {proc_par_meta_in}));
const auto out_field = std::shared_ptr<arrow::KeyValueMetadata>(
    new arrow::KeyValueMetadata({proc_par_meta}, {proc_par_meta_out}));

/// Small helper to parse a partial ObjectID given as a hexadecimal
/// string. Returns the length of the object ID.
int ParseHexObjectID(const std::string &str, plasma::ObjectID *prefix);

/// Processor base class
///
/// A processor connects to a Plasma store and waits for record
/// batches with a certain ObjectID prefix to appear. Each row in such
/// tables is interpreted as calls to the processor. The function to
/// call is given by metadata attached to the table's schema.
///
/// If a table column is itself an object ID and is marked with
/// appropriate meta dta it will be interpreted as references to
/// Plasma objects. If it is marked as "in" this means that the call
/// will only be made once the object in question is created (and
/// sealed) in the Plasma store. If it is marked as "out" this means
/// that the call will be suppressed if the object in question already
/// exists in the object store.
class Processor
{
public:
    Processor() {}
    virtual ~Processor();
    ARROW_DISALLOW_COPY_AND_ASSIGN(Processor);

    /// Connect to Plasma store for calls with given prefix
    arrow::Status Connect(std::string plasma_path,
                          plasma::ObjectID call_prefix,
                          int prefix_length);

    /// Wait for a process record to appear and process it
    arrow::Status Process();

    arrow::Status Disconnect() {
        auto status = client_->Disconnect();
        client_.reset();
        obj_table_.clear();
        return status;
    }

protected:

    template<typename T, typename Elem>
    static arrow::Status ReadValue(std::shared_ptr<arrow::Array> arr,
                                   Elem *elem)
    {
        auto arr_ = std::dynamic_pointer_cast<
            typename arrow::TypeTraits<T>::ArrayType>(arr);
        ARROW_RETURN_IF(!arr_,
            arrow::Status(arrow::StatusCode::TypeError,
                          std::string("Wrong array type - expected ") +
                          T::type_name() + ", got " + arr->type()->ToString()));
        *elem = std::move(arr_->Value(0));
        return arrow::Status::OK();
    }

    static arrow::Status ReadObjectID(std::shared_ptr<arrow::Array> arr,
                                      plasma::ObjectID *obj_id);

    arrow::Status GetParameterBuf(std::shared_ptr<arrow::RecordBatch> batch,
                                  const std::string &par_name,
                                  std::shared_ptr<arrow::Buffer> *buf);

    template<typename TYPE>
    arrow::Status GetParameterTensor(std::shared_ptr<arrow::RecordBatch> batch,
                                     const std::string &par_name,
                                     std::shared_ptr<arrow::NumericTensor<TYPE> > *out)
    {

        // Get buffer
        std::shared_ptr<arrow::Buffer> data;
        ARROW_RETURN_NOT_OK(GetParameterBuf(batch, par_name, &data));

        // Read tensor
        arrow::io::BufferReader reader(data);
        ARROW_ASSIGN_OR_RAISE(auto tensor, arrow::ipc::ReadTensor(&reader));

        // Make sure the type matches
        arrow::Type::type tid = arrow::TypeTraits<TYPE>::type_singleton()->id();
        ARROW_RETURN_IF(tensor->type_id() != tid,
                        arrow::Status(arrow::StatusCode::TypeError,
                                      "Wrong tensor type!"));

        // Convert
        ARROW_ASSIGN_OR_RAISE(*out,
            arrow::NumericTensor<TYPE>::Make(tensor->data(), tensor->shape(),
                                             tensor->strides(),
                                             tensor->dim_names()));
        return arrow::Status::OK();
    }

    virtual arrow::Status ProcessCall(const std::string &proc_func,
                                      std::shared_ptr<arrow::RecordBatch> batch) = 0;

    // Client connection to Plasma
    std::shared_ptr<plasma::PlasmaClient> client_;

private:

    /// Read calls from given ID in Plasma
    arrow::Status ReadCalls(const plasma::ObjectID &object_id);

    /// Process single call from a record batch
    arrow::Status ReadCall(const plasma::ObjectID &call_oid,
                           std::shared_ptr<arrow::RecordBatch> batch,
                           const std::vector<int> &in_obj_cols,
                           const std::vector<int> &out_obj_cols);

    /// Extract input and output object parameters from schema
    static void FindInOutPars(std::shared_ptr<arrow::Schema> schema,
                              std::vector<int> *in_obj_cols,
                              std::vector<int> *out_obj_cols);

    // Prefix of processing requests addressed to us
    plasma::ObjectID prefix_;
    int prefix_length_;
    // File descriptor subscribed to object updates
    int sub_fd_;
    // Known objects in Plasma.
    plasma::ObjectTable obj_table_;
    // Calls waiting for inputs (input oid -> call oid, record batch)
    using DelayedCall = std::pair<plasma::ObjectID,
                                  std::shared_ptr<arrow::RecordBatch> >;
    std::unordered_multimap<plasma::ObjectID, DelayedCall> delayed_calls_;
};
