import numpy as np
import pyarrow as pa
import pyarrow.plasma as plasma


class BindingPrototype(object):

    def __init__(self):
        pass

    def get_schema(self) -> pa.Schema:
        pass

    def get_buffer_size(self) -> int:
        pass

    def to_buffer(self, value, buffer: plasma.PlasmaBuffer):
        pass


class UVWPrototypeBinding(BindingPrototype):

    def __init__(self):
        super().__init__()

    def get_schema(self) -> pa.Schema:
        u_type = pa.float64()
        u_field = pa.field(name='u', type=u_type, bool_nullable=False)
        v_type = pa.float64()
        v_field = pa.field(name='v', type=v_type, bool_nullable=False)
        w_type = pa.float64()
        w_field = pa.field(name='w', type=w_type, bool_nullable=False)
        return pa.schema([u_field, v_field, w_field])

    def to_buffer(self, value, buffer: plasma.PlasmaBuffer):
        if value is np.ndarray:
            u = value[0]


class PrototypedSerializer:

    _binding: BindingPrototype

    def __init__(self, schema: BindingPrototype):
        self._binding = schema

    def get_buffer_size(self) -> int:
        size = 0
        for data_type in self._binding.get_schema().types:
            size += data_type.bit_width
        return size

    def to_buffer(self, value, buffer: plasma.PlasmaBuffer):
        self._binding.to_buffer(value=value, buffer=buffer)


