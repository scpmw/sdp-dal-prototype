from typing import List


class DataTable(object):
    """
    Meta description of a data table
    """

    _name: str

    def __init__(self, name: str):
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value


class DataType(object):
    """
    Data type
    """

    _name: str

    def __init__(self, name: str):
        self._name = name

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value


class MetaDataType(DataType):
    """
    Meta data type
    """
    def __init__(self, name: str):
        super().__init__(name=name)


class BulkDataType(DataType):
    """
    Bulk data type
    """

    _size: int

    def __init__(self, name: str, size: int):
        super().__init__(name=name)
        self._size = size

    @property
    def size(self) -> int:
        return self._size

    @size.setter
    def size(self, value: int):
        self._size = value


class BulkDataArray(DataTable):
    """
    Bulk Data Array
    """
    _dimensions: int
    _bulk_data_type: BulkDataType

    def __init__(self, name: str, dimensions: int, bulk_data_type: BulkDataType):
        super().__init__(name=name)
        self._dimensions = dimensions
        self._bulk_data_type = bulk_data_type

    @property
    def dimensions(self) -> int:
        return self._dimensions

    @dimensions.setter
    def dimensions(self, value: int):
        self._dimensions = value

    @property
    def bulk_data_type(self) -> BulkDataType:
        return self.bulk_data_type

    @bulk_data_type.setter
    def bulk_data_type(self, value: BulkDataType):
        self._bulk_data_type = value


class Column(object):
    """
    Column descriptor
    """
    _name: str
    _data_type: DataType

    def __init__(self, name: str, data_type: DataType):
        self._name = name
        self._data_type = data_type

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value

    @property
    def data_type(self) -> DataType:
        return self._data_type

    @data_type.setter
    def data_type(self, value: DataType):
        self._data_type = value


class MetaDataTable(DataTable):
    """
    Meta Data Table
    """
    _columns: List[Column]
    _bulk_data_arrays: List[BulkDataArray]

    def __init__(self, name: str, columns: List[Column], bulk_data_arrays: List[BulkDataArray]):
        super().__init__(name=name)
        self._columns = columns
        self._bulk_data_arrays = bulk_data_arrays

    @property
    def columns(self) -> List[Column]:
        return self._columns

    @columns.setter
    def columns(self, value: List[Column]):
        self._columns = value

    @property
    def bulk_data_arrays(self) -> List[BulkDataArray]:
        return self._bulk_data_arrays

    @bulk_data_arrays.setter
    def bulk_data_arrays(self, value: List[BulkDataArray]):
        self._bulk_data_arrays = value


class DataModel(object):
    """
    Root element of the meta model
    """

    def __init__(self, data_tables: List[DataTable]):
        self._data_tables = data_tables

    @property
    def data_tables(self) -> List[DataTable]:
        return self._data_tables

    @data_tables.setter
    def data_tables(self, value: List[DataTable]):
        self._data_tables = value
